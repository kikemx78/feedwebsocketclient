export interface IOddFlatten {
    id: any;
    bet: any;
    line: any;
    price: any;
    sport: any;
    main: any;
    order: any;
    column: any;
    status: any;
    status_: any;
    display: any;
    event: any;
    american: any;
    replacers: any;
    suspended: any;
    outcomeId: any;
    oddIsLive: any;
    bookmakerId: any;
    marketPrice: any;
    asian: boolean;
    lastupdate: any;
  }