import config from './config';
import { asianOdds } from './constants';
import { IOddFlatten } from './interfaces';
const logger = require('pino')({ enabled: !!process.env.NOLOG } );
let backendParserLogger = logger.child({type: 'backend_parser'});

export class BackendOddsMessageParser {
  static ParseData(requestData: any): IOddFlatten[] {
    let rval = [];
    for (let externaloddsid in requestData) {
      let oddsData = requestData[externaloddsid];
      if (!oddsData.outcomeid) {
        backendParserLogger.info('WARNING: odds object is missing required outcomeid field');
        continue;
      }
      if (!oddsData.eventid) {
        backendParserLogger.info('WARNING: odds object is missing required eventid field');
        continue;
      }
      if (!oddsData.sportid) {
        backendParserLogger.info('WARNING: odds object is missing required sportid field');
        continue;
      }
      if (!oddsData.status) {
        backendParserLogger.info('WARNING: odds object is missing required status field');
        continue;
      }
      if (!oddsData.bookmakerid) {
        backendParserLogger.info('WARNING: odds object is missing required bookmakerid field');
        continue;
      }
      if (typeof oddsData.currentprice === 'undefined') {
        backendParserLogger.info('WARNING: odds object is missing required currentprice field');
        continue;
      }
      if (typeof oddsData.market_price === 'undefined') {
        backendParserLogger.info('WARNING: odds object is missing required market_price field');
        continue;
      }
      if (typeof oddsData.display === 'undefined') {
        backendParserLogger.info('WARNING: odds object is missing required display field');
        continue;
      }
      if (typeof oddsData.round_data === 'undefined') {
        backendParserLogger.info('WARNING: odds object is missing required round_data field');
        continue;
      }
      if (typeof oddsData.lastupdate === 'undefined') {
        backendParserLogger.info('WARNING: odds object is missing required lastupdate field');
        continue;
      }

      const id = externaloddsid;
      const eventId = +oddsData.eventid;
      const sport = oddsData.sportid;
      const status = oddsData.status;
      const status_ = oddsData.status;
      let bookmakerId = oddsData.bookmakerid;
      const outcomeId = oddsData.outcomeid;
      const oddIsLive = oddsData.islive === 1 ? 1 : 0;
      bookmakerId = oddIsLive === 1 ? config.mockBookmaker : bookmakerId; // This is done to make it work with mock bookmakerId data
      const bet = oddsData.bet;
      const price = +oddsData.currentprice;
      const marketPrice = +oddsData.market_price;
      const american = oddsData.price_american || '';
      const line = oddsData.line || '';
      const display = oddsData.display.split('/').join(' / ');
      const isLST = oddsData.lst || false;
      const isAsian = asianOdds.indexOf(+outcomeId) >= 0;
      const order = typeof oddsData.row !== 'undefined' ? +oddsData.row : -1;
      const column = typeof oddsData.column !== 'undefined' ? +oddsData.column : -1;
      const replacers = oddsData.round_data.split('|');
      const suspended = status === 'TemporarilySuspended';
      const lastupdate = oddsData.lastupdate * 1000.0;
      let oddObj: IOddFlatten = {
        id,
        bet,
        price,
        marketPrice,
        american,
        line,
        main: isLST,
        asian: isAsian,
        order,
        column,
        replacers,
        suspended,
        display,
        event: eventId,
        sport,
        status: status !== 'AdministrativelySuspendedLTD' ? undefined : status,
        status_,
        bookmakerId,
        outcomeId,
        oddIsLive,
        lastupdate
      };
      rval.push(oddObj);
    }

    return rval;
  }
}