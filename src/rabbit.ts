let amqp = require('amqplib/callback_api');
const CONN_URL = 'amqp://localhost';

function CreateRabbit() {

  return new Promise((res, rej) => {

    amqp.connect(CONN_URL, function (err, conn) {
      
      if (err) {
        rej(err);
      }

      conn.createChannel(function (err, channel) {
        
        if (err) {
          rej(err);
        }

        res(channel);
      });

   });

  });
};

export default CreateRabbit;
