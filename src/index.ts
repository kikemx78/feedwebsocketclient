import * as WebSocket from 'ws';
import * as pino from 'pino';
import config from './config';

import CreateRabbit from './rabbit';
import { updateOutcomes } from './updateOutcomes';
import { BackendOddsMessageParser } from './backend_odds_message_parser';

const logger: any = pino();
const feedWebsocketLog = logger.child({type: 'web_socket_start'});

export const cachedData: {
  flattenOutcomes: any,
  lstOutcomes: any,
  outcomes: any
} = {
  flattenOutcomes: {
    allPrematchOddsByEvent: {},
    mainPrematchOddsBySport: {},
    asianOddsBySport: {},
    allOddsByEvent: {},
    mainOddsBySport: {}
  },
  lstOutcomes: {},
  outcomes: {}
};

class OutcomesFeedService {

    private timeOut: any = null;
    private intervalPing: any = null;
  
    private which: any;
    private server: any;
    private backendClient: any;
  
    private connectivityStatus: boolean = false;
    private haveDataFetchIntervalsRunning: boolean = false;
  
    private gamecastWatchlist: string[] = [];
  
    private closedFeedEventCounter: number = 0;
    private lastMarketCountUpdate: number = 0;
    private lastServerConnectionMsgToConnectedUsers: number = 0;
    private lastFeedMessage: Date = new Date(0);
    private foundDisconnectEvent: boolean = false;

    private channel: any;

    constructor(which: string, channel: any) {
        
        this.channel = channel;

        this.which = which;
        let that: any = this;
        console.log(this.channel);
        if (!this.haveDataFetchIntervalsRunning) {

            this.timeOut = setInterval(function() {
          
              const now = new Date().getTime();
      
              if (that.foundDisconnectEvent) {
                that.foundDisconnectEvent = false;
                feedWebsocketLog.warn('restarting connection after close event');
      
                feedWebsocketLog.info(`Calling start() to re-start connection to ${that.which} ws`);
                that.start();
      
                // We just called that.start(), meaning we are going to try to start a WS connection.
                // We return now to leave this function so that we don't ALSO slip into the 'Check connectivity status' below.
                return;
              }
      
              // Check connecitvity status
              if (now - that.lastFeedMessage.getTime() > 60000) {
                that.connectivityStatus = false;
      
                feedWebsocketLog.error(`${new Date().toString()} Flagging ${that.which} connectivity status as a problem - last activity was ${now - that.lastFeedMessage.getTime()} ms ago` );
                that.closedConnection(504, `${that.which} - Last activity was ${now - that.lastFeedMessage.getTime()} ms ago`, that.which);
              }
      
            }, 10000);
      
            this.haveDataFetchIntervalsRunning = true;
        }

        feedWebsocketLog.info(`instantiating ${which} socket client`);
        this.start();

      
    }

    start() {
        
        this.channel.assertQueue('odds', {
          durable: false
        });

        clearInterval(this.intervalPing);

        let client = this.backendClient;

        if (client) {
            feedWebsocketLog.info(`start() - Closing connection on ${this.which} before starting the new connection`);
            client.terminate();
            client = null;
        }

        let feedURL: any = {
            'live': config.liveOddsWebsocketUrl,
            'prematch': config.prematchOddsWebsocketUrl
        };

        const ip = feedURL[this.which];
        client = new WebSocket(ip);
        this.backendClient = client;
        this.connectivityStatus = true;
        this.lastFeedMessage = new Date();

        let that: any = this;

        this.intervalPing = setInterval(() => {
            that.ping(that);
        }, 10000);

        feedWebsocketLog.info(`${new Date().toString()} Starting WS connection to ${ip}; for ${this.which}`);

        client.on('open', function() {

            feedWebsocketLog.info(`${new Date().toString()} connection to ${that.which} feed established - sending login`);
            that.lastFeedMessage = new Date();
            that.doFeedAuthentication();
      
            if (that.which !== 'live') {
              // Uncomment to replicate Aprubte Disconnection event at an interval
              // that.testInterval(client);
            }
            
            setInterval(() => {
              // that.channel.sendToQueue('odds', Buffer.from(JSON.stringify(cachedData)));
            }, 5000);
      
        });

        client.on('message', function(msg: any) {
            that.handleMessage(msg, that.which);
        });

        client.on('error', function(err: any) {
            feedWebsocketLog.error(`Caught WebSocket error for ${that.which}: ${err}`);
            that.closedConnection(505, `error received on client ${that.which}: ${err}`);
        });
    }

    doFeedAuthentication() {

        if (this.backendClient && this.backendClient.readyState === this.backendClient.OPEN) {
    
          let credentials: any = {
            'live': {
              username: config.liveOddsUsername,
              password: config.liveOddsPassword
            },
            'prematch': {
              username: config.prematchOddsUsername,
              password: config.prematchOddsPassword
            }
          };
    
          const loginMessage = {
            requestType: 'login',
            data: {
              username: credentials[this.which].username,
              password: credentials[this.which].password
            }
          };
          this.backendClient.send(JSON.stringify(loginMessage));

        }
        else {
          feedWebsocketLog.error('ERROR in doFeedAuthentication: specified client is not connected or readyState is not OPEN');
        }
    
    }

    ping(that: any) {
        that.sendPingMessage();
    }

    sendPingMessage() {
        if (this.backendClient.readyState !== this.backendClient.OPEN) {
          feedWebsocketLog.error(`error from ping ${this.backendClient.readyState}`);
          this.closedConnection(0, `Connection close called by ping() for ${this.which}`);
        } else {
          feedWebsocketLog.error(`backendClient ping ${this.which}`);
          this.backendClient.send(JSON.stringify({
            requestType: 'ping',
            data: {}
          }));
        }
    }

    closedConnection(code: any, reason: any) {

        feedWebsocketLog.warn(`Close detected.  Which:  ${this.which}, [code] ${code}, [reason] ${reason}`);
        this.foundDisconnectEvent = true;
    
        feedWebsocketLog.warn(`Clearing cached data from ${this.which} socket`);
    
        this.closedFeedEventCounter ++;
        feedWebsocketLog.error(`Abrupte disconnection ${this.which} No. ${this.closedFeedEventCounter}`);
    
    }

    handleMessage(msg: any) {

        this.lastFeedMessage = new Date();
        this.connectivityStatus = true;
    
        const received = JSON.parse(msg);
    
        let gamecastWatchlist = null;
    
        switch (received.requestType) {  
          case 'odds':
            
            const oddsData = received.data || {};
            const parsedOdds = BackendOddsMessageParser.ParseData(oddsData);
            updateOutcomes(cachedData.outcomes, cachedData.lstOutcomes, parsedOdds, this.channel);
            break;
          case 'gamecast':
          case 'gamecastpush':
            if (!received.data) break;
            break;
          case 'events':
            break;
          case 'pong':
            feedWebsocketLog.info(`pong for ${this.which}`);
            // Pong messages from server are in reply to our ping; we don't need to do anything with them
            break;
          default:
            console.log(`Unknown message: `, msg);
            break;
        }
    
        let currentTime = new Date().getTime();
    
        if ( (currentTime - this.lastServerConnectionMsgToConnectedUsers) > 10000) {
          this.sendBackendConnectivityStatusToConnectedUsers();
          this.lastServerConnectionMsgToConnectedUsers = currentTime;
        }
    
        if (received.command === 'timeout') {
          this.start();
        }
    
    }

    sendBackendConnectivityStatusToConnectedUsers() {
        let isClosedConnection = this.haveBackendConnectivityProblem();
        // feedWebsocketLog.info(`Telling clients that ${this.which} has disconnected`);
    }

    haveBackendConnectivityProblem() {
        let feedOK = true;
        if (!this.connectivityStatus) {
          feedOK = false;
        }
    
        feedWebsocketLog.info(`HaveBackendConnectivityProblem is being evaluated. [${this.which}Ok] ${this.which}; [feedOK] ${feedOK}`);
    
        if (!feedOK) return true;
        return false;
    }
    

}

async function CreateFeedInstance(which: string) {

  CreateRabbit()
    .then((channel: any) => {
      new OutcomesFeedService(which, channel);
    })
    .catch((err: any) => {
      console.log(`Error instantiating rabbit ${err}`);
    });
}

CreateFeedInstance('live');
