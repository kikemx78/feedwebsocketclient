import { IFlattenOutcomes, IOddFlatten, IUpdateOutcome, IDeleteOutcome, IClientState } from './interfaces';

// ** This class is used to manage the outcomes object server-side. It's a simple ADD-UPDATE-DELETE system.
// ** We are using a 'direct' approach instead of an immutable one because this latter is more CPU demanding

class OutcomesDataHandlerServer {

// ** We organize outcomes inside a 'flattenOutcomes' object. This object's childs are:
// ** allOddsByEvent, mainOddsBySport, asianOddsBySport, mainPrematchOddsBySport, allPrematchOddsByEvent
// **
// ** A given outcome can belong to more than one child e.g. allOddsByEvent, mainOddsBySport, asianOddsBySport
// ** 'getOddsKeys' function returns an array containing the keys a given outcome belongs to.
// ** e.g ['allOddsByEvent', 'mainOddsBySport', 'asianOddsBySport']
// ** The param odd is the outcome in question.
// **  */

  static getOddsKeys(odd: IOddFlatten | IUpdateOutcome | IDeleteOutcome) {

    const { main, asian, oddIsLive }: any = odd;

    let oddsKeys = oddIsLive === 1 ? ['allOddsByEvent'] : ['allPrematchOddsByEvent'];
    oddsKeys = oddIsLive === 1 && main ? [...oddsKeys, 'mainOddsBySport'] : oddsKeys;
    oddsKeys = asian ? [...oddsKeys, 'asianOddsBySport'] : oddsKeys;
    oddsKeys = oddIsLive === 0 && main ? [...oddsKeys, 'mainPrematchOddsBySport'] : oddsKeys;

    return oddsKeys;

  }

// ** We are adding an outcome to the flattenOutcomes[child1][child2] object it belongs to e.g. flattenOutcomes.mainOddsBySport[sport][odd.id]
// ** The function returns the whole flattenOutcomes object

  static createOutcome(state: any, odd: IOddFlatten) {
    // console.log(odd);

    let { id, sport, event }: any = odd;

    let oddsKeys: any = this.getOddsKeys(odd);

    oddsKeys
      .forEach((odds: any) => {
        let key: any = odds === 'mainOddsBySport' || odds === 'asianOddsBySport' || odds === 'mainPrematchOddsBySport' ? sport : event;

        if (!state[odds][key]) {
          state[odds][key] = {};
        }

        state[odds][key][id] = odd;

      });

    return state;

  }

  // ** Updates an outcome. The only values that can be updated are price, american, marketPrice */

  static updateOutcome(state: any, updatePayload: IUpdateOutcome) {

    const { event, oddId, updateObj, sport } = updatePayload;

    if (!updateObj) return state;

    let oddsKeys = this.getOddsKeys(updatePayload);

    oddsKeys
      .forEach(odds => {
        let key = odds === 'mainOddsBySport' || odds === 'asianOddsBySport' || odds === 'mainPrematchOddsBySport' ? sport : event;

        // If we have a disconnect event from ws feed and this process is running. We can get errors, that's why we need to check the odd exists.

        if (state[odds] && state[odds][key] && state[odds][key][oddId]) {
          state[odds][key][oddId].price = updateObj.price || state[odds][key][oddId].price;
          state[odds][key][oddId].american = updateObj.american || state[odds][key][oddId].american;
          state[odds][key][oddId].marketPrice = updateObj.marketPrice || state[odds][key][oddId].marketPrice;
        }

      });

    return state;

  }

  // ** Deletes an outcome */
  static deleteOutcome(state: any, deletePayload: IDeleteOutcome) {

    const { oddId, event, sport, oddIsLive }: any = deletePayload;

    let oddsKeys = this.getOddsKeys(deletePayload);

    oddsKeys
      .forEach(odds => {
        let key = odds === 'mainOddsBySport' || odds === 'asianOddsBySport' || odds === 'mainPrematchOddsBySport' ? sport : event;
        // If there is a disconnect event, cachedData is emptied, if this event happens during this deleteOutcome proccess,
        // there'll be no outcome to delete

        if (state[odds] && state[odds][key] && state[odds][key][oddId]) {
          delete state[odds][key][oddId];
        }

      });

    return state;

  }

}

export default OutcomesDataHandlerServer;
