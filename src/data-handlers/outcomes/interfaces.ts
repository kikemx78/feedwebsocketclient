export interface IFlattenOutcomes {
    allOddsByEvent: any;
    mainOddsBySport: IBySport;
    asianOddsBySport: IBySport;
    allPrematchOddsByEvent: any;
    mainPrematchOddsBySport: IBySport;
  }
  
  export interface IBySport {
    [id: number]: {
      [oddId: string]: any;
    };
  }
  
  export interface IOddFlatten {
    id: any;
    bet: any;
    line: any;
    price: any;
    sport: any;
    main: any;
    order: any;
    column: any;
    status: any;
    status_: any;
    display: any;
    event: any;
    american: any;
    replacers: any;
    suspended: any;
    outcomeId: any;
    oddIsLive: any;
    bookmakerId: any;
    marketPrice: any;
    asian: boolean;
    lastupdate: any;
  }
  
  export interface IUpdateOutcome {
    oddId: string;
    sport: string;
    updateObj: any;
    main: boolean;
    asian: boolean;
    event: number;
    oddIsLive: number;
  }
  
  export interface IDeleteOutcome {
    oddId: string;
    event: number;
    sport: string;
    main: boolean;
    asian: boolean;
    oddIsLive: number;
  }
  
  export interface IClientState {
    isFromLive: boolean;
    msgType: any;
  }
  