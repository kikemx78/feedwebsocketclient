import { cachedData } from './index';
import { batchGenerator } from './batch-generator';
import OutcomesDataHandlerServer from './data-handlers/outcomes/server';
import { IUpdateOutcome, IDeleteOutcome } from './data-handlers/outcomes/interfaces';

const now = () => +new Date();

let timeOut: any;
let lastUpdate = now();
let updateBatch: any = {};
const INTERVAL_BETWEEN_UPDATES = 1500;
let evs: { [eventId: string]: any } = {};
let lstEvs: { [eventId: string]: any } = {};

export const usedStatuses = ['Open', 'TemporarilySuspended', 'AdministrativelySuspendedLTD'];

export function updateOutcomes(currentOutcomes: any, lstOutcomes: any, incomingOdds: any, channel: any) {
	
	const killedLstOutcomes: any = [];
	const killedNonLstOutcomes: any = [];
	let haveWrittenLog = false;
  
  incomingOdds.forEach(ev => {
  
		const eventId = ev.event;
		const bookmakerId = ev.bookmakerId;
		const outcomeId = ev.outcomeId;
		const isLST = ev.main;
		const isAsian = ev.asian;
		const id = ev.id;
		const sport = ev.sport;
		const lastUpdate = ev.lastupdate;
		const status = ev.status_;
		const status_ = ev.status_;
		const display = ev.display;
		const oddIsLive = ev.oddIsLive;
		const american = ev.american;
		const price = ev.price;
		const marketPrice = ev.marketPrice;
		const bet = ev.bet;
		const line = ev.line;
		const suspended = ev.suspended;
  
		let oddObj: any = ev;

		if (!currentOutcomes[eventId]) {
			currentOutcomes[eventId] = {};
		}
		if (!lstOutcomes[eventId]) {
			lstOutcomes[eventId] = {};
		}
  
		const eventOutcomes = currentOutcomes[eventId];
		const eventLSTOutcomes = lstOutcomes[eventId];

		if (eventOutcomes && !eventOutcomes[bookmakerId]) {
			eventOutcomes[bookmakerId] = {};
			eventLSTOutcomes[bookmakerId] = {};
		}
  
		if (eventOutcomes && eventOutcomes[bookmakerId] && !eventOutcomes[bookmakerId][outcomeId]) {
			eventOutcomes[bookmakerId][outcomeId] = {};

			if (eventLSTOutcomes && eventLSTOutcomes[bookmakerId]) {
				eventLSTOutcomes[bookmakerId][outcomeId] = {};

				if (isLST || isAsian) {
					eventLSTOutcomes[bookmakerId][outcomeId] = eventOutcomes[bookmakerId][outcomeId];
				}

			}

		}
  
    const outcome = eventOutcomes[bookmakerId][outcomeId];
  
    let shouldUpdate = false;
  
		if (!haveWrittenLog) {
			haveWrittenLog = true;
			const feedLastUpdate = lastUpdate;
			const feedDate = new Date(feedLastUpdate);
			const currentDateTime = new Date().toUTCString();

			// dataTracker['lastSuccesfulOddsUpdateDelta'] = Date.now() - feedLastUpdate;
			// dataTracker['timeOfLastSuccessfulOddsUpdateFromFeed'] = feedLastUpdate;

		}
  
		let updateObj: any = {};
		let type_ = '';
		
		if (usedStatuses.indexOf(status) !== -1) {
			const odd = outcome[id];

			if (!odd || (odd.price !== ev.price || odd.american !== ev.american || odd.suspended !== ev.suspended || odd.status !== status)) {

				// oddsMonitorBuilder(odd, id, ev.status, ev.oddIsLive, ev.display, eventId, outcomeId);

				outcome[id] = {
					order: ev.order,
					column: ev.column,
					price: ev.price,
					american: ev.american,
					display: ev.display,
					suspended: ev.suspended,
					replacers: ev.replacers,
					marketPrice: ev.marketPrice,
					line: ev.line,
					bet: ev.bet,
					asian: isAsian,
					main: isLST,
					status_: status,
					status: status !== 'AdministrativelySuspendedLTD' ? undefined : status,
					oddIsLive: ev.oddIsLive,
					bookmakerId,
					outcome: outcomeId,
					id,
					event: eventId,
					sport: ev.sport,
					lastupdate: ev.lastupdate
				};
				shouldUpdate = true;
			}

			const oddFlatten = oddIsLive === 1 ? cachedData.flattenOutcomes.allOddsByEvent[eventId] && cachedData.flattenOutcomes.allOddsByEvent[eventId][id] : cachedData.flattenOutcomes.allPrematchOddsByEvent[eventId] && cachedData.flattenOutcomes.allPrematchOddsByEvent[eventId][id];

			if (!oddFlatten || (oddFlatten.price !== ev.price || oddFlatten.american !== ev.american || oddFlatten.suspended !== ev.suspended || oddFlatten.status !== status)) {

				if (!oddFlatten) {

					cachedData.flattenOutcomes = OutcomesDataHandlerServer
						.createOutcome(cachedData.flattenOutcomes, oddObj);

					type_ = 'ADD';

					oddObj['ts'] = Date.now();
					updateObj = {
						...updateObj,
						[id]: oddObj
					};
				}
				// console.log(oddFlatten);
				if (oddFlatten) {

					[
						{ condition: oddFlatten.line !== line, key: 'line', oldKey: 'oldLine', value: line, oldValue: oddFlatten.line },
						{ condition: oddFlatten.price !== price, key: 'price', oldKey: 'oldPrice', value: price, oldValue: oddFlatten.price },
						{ condition: oddFlatten.status_ !== status, key: 'status', oldKey: 'oldStatus', value: status, oldValue: oddFlatten.status_ },
						{ condition: oddFlatten.display !== display, key: 'display', oldKey: 'oldDisplay', value: display, oldValue: oddFlatten.display },
						{ condition: oddFlatten.american !== american, key: 'american', oldKey: 'oldAmerican', value: american, oldValue: oddFlatten.american },
						{ condition: oddFlatten.suspended !== suspended, key: 'suspended', oldKey: 'oldSuspended', value: suspended, oldValue: oddFlatten.suspended },
						{ condition: oddFlatten.marketPrice !== marketPrice, key: 'marketPrice', oldKey: 'oldmarketPrice', value: marketPrice, oldValue: oddFlatten.marketPrice }

					].forEach(item => {

						if (item.condition) {

							type_ = 'UPDATE';

							updateObj = {
								...updateObj,

								[id]: {
									...updateObj[id],
									[item.key]: item.value,
									eventId,
									ts: Date.now()

								}
							};

						}

					});

					let updateOutcomePayload: IUpdateOutcome = {
						updateObj: updateObj[id],
						oddIsLive,
						oddId: id,
						event: eventId,
						asian: isAsian,
						main: isLST,
						sport

					};

					cachedData.flattenOutcomes = OutcomesDataHandlerServer
						.updateOutcome(cachedData.flattenOutcomes, updateOutcomePayload);

				}
				// console.log(JSON.stringify(cachedData.flattenOutcomes));
			}

		}
		else { // this means status is suspended or something else. So we need to delete the odd from UI's outcome state
			if (outcome[id]) {
				shouldUpdate = true;

				// console.log(status_, 'status_ before delete');

				let deleteOutcomePayload: IDeleteOutcome = {
					oddId: id,
					oddIsLive,
					asian: isAsian,
					event: eventId,
					main: isLST,
					sport

				};

				cachedData.flattenOutcomes = OutcomesDataHandlerServer
					.deleteOutcome(cachedData.flattenOutcomes, deleteOutcomePayload);

				type_ = 'DELETE';

				updateObj = {
					...updateObj,
					[id]: {
						...updateObj[id],
						ts: Date.now()
					}
				};

				if (eventOutcomes[bookmakerId] && eventOutcomes[bookmakerId][outcomeId] && eventOutcomes[bookmakerId][outcomeId].hasOwnProperty(id)) {
					killedNonLstOutcomes.push([id, outcomeId, eventId, bet, line, status]);
					const odd = eventOutcomes[bookmakerId][outcomeId][id];
					// oddsMonitorBuilder(odd, id, ev.status, ev.oddIsLive, ev.display, eventId, outcomeId);
					delete eventOutcomes[bookmakerId][outcomeId][id];

				}

				if ((isLST || isAsian) && eventLSTOutcomes[bookmakerId] && eventLSTOutcomes[bookmakerId][outcomeId] && eventLSTOutcomes[bookmakerId][outcomeId].hasOwnProperty(id)) {
					killedLstOutcomes.push([id, outcomeId, eventId, bet, line, status]);
					const odd = eventLSTOutcomes[bookmakerId][outcomeId][id];
					// oddsMonitorBuilder(odd, id, ev.status, ev.oddIsLive, ev.display, eventId, outcomeId);
					delete eventLSTOutcomes[bookmakerId][outcomeId][id];

				}
			}
		}
  
		if (JSON.stringify(updateObj) !== '{}') {

			if (oddIsLive === 1) {

				if (isLST) {
					updateBatch = batchGenerator(updateObj, updateBatch, oddObj, id, 'main', sport, type_);
				}

				if (isAsian) {
					updateBatch = batchGenerator(updateObj, updateBatch, oddObj, id, 'asian', sport, type_);
				}

			} else {
				if (isLST) {
					updateBatch = batchGenerator(updateObj, updateBatch, oddObj, id, 'mainPrematch', sport, type_);
				}
			}

			// Regular outcomes updates for prematch and live single events are handled on the same subscribed client's object (`events-${eventId}`)
			// There is no need to create a new entity for prematch regular outcomes update batch

			updateBatch = batchGenerator(updateObj, updateBatch, oddObj, id, 'regular', eventId, type_);

		}
  
		if (shouldUpdate) {
			evs[eventId] = eventOutcomes;

			if (isLST || isAsian) {
				lstEvs[eventId] = eventLSTOutcomes;
			}

		}
  
  });
  
  const t = now() - lastUpdate;
  
  if (t >= INTERVAL_BETWEEN_UPDATES) {
  
		lastUpdate = now();


		// we send the cachedData (outcomes) here - arbitrary
		channel.sendToQueue('odds', Buffer.from(JSON.stringify(cachedData)));
		console.log('Odds state Sent %s');

		// updateBatch needs to be sent here... 

		updateBatch = {};
		
		// See how we are going to send updates now...

		evs = null;
		evs = {};
  
		lstEvs = null;
		lstEvs = {};
  
  }
  else {
		clearTimeout(timeOut);
		timeOut = setTimeout(() => updateOutcomes(currentOutcomes, lstOutcomes, [], channel), INTERVAL_BETWEEN_UPDATES);
  }
  
	// Write log entry about the deleted odds.
	const genDeletedOddsLogString = function(singleEntry: any, lstFlag: any) {
		const _id = singleEntry[0];
		const _outcomeId = singleEntry[1];
		const _eventId = singleEntry[2];
		const _bet = singleEntry[3];
		const _line = singleEntry[4];
		const _status = singleEntry[5];
		const dt = new Date();

		const generatedString = `${dt.toUTCString()} [ts] ${dt.getTime()} [id] ${_id} [outcomeId] ${_outcomeId} [eventId] ${_eventId} [bet] ${_bet} [line] ${_line} [status] ${_status} was deleted via ws/outcomes.ts updateOutcomes - ${lstFlag}`;
		// console.log(generatedString);
		return generatedString;
	};
  
	killedLstOutcomes.forEach((item: any) => {
		// outcomesLog.debug(genDeletedOddsLogString(item, 'is LST'));
	});

	killedNonLstOutcomes.forEach((item: any) => {
		// outcomesLog.debug(genDeletedOddsLogString(item, 'is NOT LST'));
	});
}