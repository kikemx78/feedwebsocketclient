let amqp = require('amqplib/callback_api');

// This will hold our outcomes state on the UI.

let outcomesState = {};

amqp.connect('amqp://localhost', function(error0, connection) {
  if (error0) {
    throw error0;
  }
  connection.createChannel(function(error1, channel) {
    
    if (error1) {
      throw error1;
    }

    let queue = 'odds';

    channel.assertQueue(queue, {
      durable: false
    });

    console.log(`[*] Waiting for ${queue}. To exit press CTRL+C`);
    channel.consume(queue, function(msg) {
      console.log(" [x] Received %s", msg.content.toString());

      outcomesState = msg.content.toString();

    }, {
      noAck: true
    });

  });
});