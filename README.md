# Outcomes Feed Service with demo workers

A demo on how to implement an outcomes micro-service and its workers.

Under `src` folder : Outcomes Feed Service: a program that starts and mantains a connection to backend feed (live and prematch); and also serves as a RabbitMQ 'Publisher' that broadcasts the outcomes state to its workers.

Under `workers` folder : Workers consume Publisher messages. Our UI can serve as a 'worker'. This may be the solution to the 'one parser many connected clients' problem. 


## Installation

Install dependencies on both  `src` and `workers` folders.

```bash
npm install
```

## Usage

You need to install RabbitMQ and start a local server to make this work :

https://bit.ly/2RxlZ0u


Run Outcomes Feed Service : under root `npm run start`

Start a worker : under ./workers `npm run start` 

You can start as many workers as you need.

The worker will be receiving the 'outcomes' state each time the feed sends an 'odds' message. 

Think of each worker as an nodeJS `sports-betting-ui-2_0` instance ; the state can be stored inside a variable and be used inside our nodeJS app.